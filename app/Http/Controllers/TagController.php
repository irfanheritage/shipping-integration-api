<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Models\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function alltags() {
     
        $tags = Tag::all();

        return response()->json($tags);

    }

    public function show_tag($id) {
        $tag = Tag::find($id);

        return response()->json($tag);
    }

    public function store(Request $request){
        $rules = [
            'name' => 'required|unique:tags|max:255'
        ];
    
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{

            $user = auth()->user();
            $input = $request->all();
            $input['created_by'] = $user->id;
            
            $post = Tag::create($input);
            if($post){
                return response()->json(['success' => 'Tag saved successfully!'], 200 );
            }

            
        }
        
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required',
            'name' => 'required|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $user = auth()->user();
            $input = $request->all();
            $input['created_by'] = $user->id;
            
            $upost = Tag::find($input['id']);
            if($upost){
                $upost->update($input);
                return response()->json(['success' => 'Tag updated successfully!'], 200 );
            }else{
                return response()->json(['error' => 'ID Not Found'], 400 );
            }

        }
        
    }

    public function destroy(Request $request){
        $rules = [
            'id' => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $upost = Tag::find($request->id);
            if($upost){
                $upost->delete();
                return response()->json(['success' => 'Tag deleted successfully!'], 200 );
            }else{
                return response()->json(['error' => 'ID Not Found'], 400 );
            }
        }
    }
}
