<?php

namespace App\Http\Controllers;

use DB;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function register(Request $request) {

        $input = $request->all();
        if(User::where('email', '=', $input['email'])->get()){
            return response()->json(['error' => 'Email already registered'], 401 );
        }else{
            $input['password'] = Hash::make($input['password']);
        
            $user = User::create($input);

            return response()->json(['success' => 'Email registered successfully!'], 200 );
        }
        
    } 

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        // Auth::logout();
        // Auth::setToken($token)->invalidate();
        auth()->logout();
        // // $this->jwt->parseToken()->invalidate();
        return response()->json(['message' => 'Successfully logged out']);
        // $this->logHistory();
        
        // try {
        // JWTAuth::invalidate(JWTAuth::getToken());
        // return response()->json([
        //     'status' => 'success',
        //     'msg' => 'You have successfully logged out.'
        // ]);
        // } catch (JWTException $e) {
        //     JWTAuth::unsetToken();
        //     // something went wrong tries to validate a invalid token
        //     return response()->json([
        //         'status' => 'error',
        //         'msg' => 'Failed to logout, please try again.'
        //     ]);
        // }
        
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}