<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function allcategories() {
     
        $cats = Category::all();

        return response()->json($cats);

    }

    public function show_category($id) {
        $cat = Category::find($id);

        return response()->json($cat);
    }

    public function store(Request $request){
        $rules = [
            'name' => 'required|unique:categories|max:255'
        ];
    
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{

            $user = auth()->user();
            $input = $request->all();
            $input['created_by'] = $user->id;
            $post = Category::create($input);
            if($post){
                return response()->json(['success' => 'Category saved successfully!'], 200 );
            }
        }
        
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required',
            'name' => 'required|max:255',
        ];
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $user = auth()->user();
            $input = $request->all();
            $input['created_by'] = $user->id;
            
            $upost = Category::find($input['id']);
            if($upost){
                $upost->update($input);
                return response()->json(['success' => 'Category updated successfully!'], 200 );
            }else{
                return response()->json(['error' => 'ID Not Found'], 400 );
            }

        }
        
    }

    public function destroy(Request $request){
        $rules = [
            'id' => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $upost = Category::find($request->id);
            if($upost){
                $upost->delete();
                return response()->json(['success' => 'Category deleted successfully!'], 200 );
            }else{
                return response()->json(['error' => 'ID Not Found'], 400 );
            }
        }
    }
}
