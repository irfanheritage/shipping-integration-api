<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Post;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Http\Request;
use Validator;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TagController;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $categoryController;
    protected $tagController;
    public function __construct(
        CategoryController $categoryController,
        TagController $tagController
        )
    {
        $this->middleware('auth:api');
        $this->categoryController = $categoryController;
        $this->tagController = $tagController;
    }

    public function allposts() {
     
        $posts = Post::all();
        $allposts = [];
        for ($i = 0; $i<count($posts); $i++){
            $categories = DB::table('post_category')->where('post_id', '=', $posts[$i]->id)->get('category_id')->toArray();
            $tags = DB::table('post_tag')->where('post_id', '=', $posts[$i]->id)->get('tag_id')->toArray();
            $allposts[$i] = $posts[$i];
            $allposts[$i]['categories'] = $categories;
            $allposts[$i]['tags'] = $tags;
        }

        return response()->json($allposts);

    }

    public function show_post($id) {
        $post = Post::find($id);
        $categories = DB::table('post_category')->where('post_id', '=', $post->id)->get('category_id')->toArray();
        $tags = DB::table('post_tag')->where('post_id', '=', $post->id)->get('tag_id')->toArray();
        $arrCat = [];
        foreach($categories as $cat){
            $catarr = $this->categoryController->show_category($cat->category_id);
            array_push($arrCat, $catarr->original);
        }
        $post->categories = $arrCat;
        $arrTag = [];
        foreach($tags as $tag){
            $tagarr = $this->tagController->show_tag($tag->tag_id);
            array_push($arrTag, $tagarr->original);
        }
        $post->tags = $arrTag;
        return response()->json($post);
    }

    public function show_post_by_tag($id){
        $tags = DB::table('post_tag')->where('tag_id', '=', $id)->get('post_id')->toArray();
        $arr = [];
        foreach($tags as $tag){
            $newArr = [];
            if($tag->post_id !== null){
                $newArr = $this->show_post($tag->post_id);
                array_push($arr, $newArr->original);
            }
        }
        return response()->json($arr);

    }

    public function show_post_by_category($id){
        $cats = DB::table('post_category')->where('category_id', '=', $id)->get('post_id')->toArray();
        $arr = [];
        foreach($cats as $cat){
            $newArr = [];
            if($cat->post_id !== null){
                $newArr = $this->show_post($cat->post_id);
                array_push($arr, $newArr->original);
            }
        }
        return response()->json($arr);

    }

    public function store(Request $request){
        $rules = [
            'title' => 'required|unique:posts|max:255',
            'content' => 'required',
            'categories' => 'required',
            'categories.*' => 'required|min:1|integer',
            'tags' => 'required',
            'tags.*' => 'required|min:1|integer'
        ];
    
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $user = auth()->user();
            $input = $request->all();
            $input['created_by'] = $user->id;
            // return $input['categories'];exit();
            $category = Category::findMany($input['categories']);
            $tag = Tag::findMany($input['tags']);

            // return $category;exit();
            if($category && $tag){

                $post = Post::create($input);

                $post->categories()->attach($category);
                $post->tags()->attach($tag);
                return response()->json(['success' => 'Post saved successfully!'], 200 );

            }else{
                return response()->json(['error' => 'Category / Tag not found!'], 401 );

            }

        }
        
    }

    public function update(Request $request){
        $rules = [
            'id' => 'required',
            'title' => 'required|max:255',
            'content' => 'required',
            'categories' => 'required',
            'categories.*' => 'required|min:1|integer',
            'tags' => 'required',
            'tags.*' => 'required|min:1|integer'
        ];
        // return $request;exit();
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $user = auth()->user();
            $input = $request->all();
            $input['created_by'] = $user->id;
            // return $input['categories'];exit();
            $category = Category::findMany($input['categories']);
            $tag = Tag::findMany($input['tags']);

            // return $category;exit();
            if($category && $tag){

                // $post = Post::create($input);
                // $post->update($input);
                $upost = Post::find($input['id']);
                if($upost){
                    $upost->update($input);
                    $upost->categories()->sync($category);
                    $upost->tags()->sync($tag);
                    return response()->json(['success' => 'Post updated successfully!'], 200 );
                }else{
                    return response()->json(['error' => 'ID Not Found'], 400 );
                }
                

            }else{
                return response()->json(['error' => 'Category / Tag not found!'], 401 );

            }

        }
        
    }

    public function destroy(Request $request){
        $rules = [
            'id' => 'required'
        ];
        // return $request;exit();
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) {
            // return response()->json($validator);
            $messages = $validator->messages();
            $error = '';
            foreach ($messages->all(':message') as $message)
            {
                 $error .= $message;
            }
            return response()->json(['error' => $error], 400);


        } else{
            $category = Category::findMany($request->categories);
            $tag = Tag::findMany($request->tags);

            // return $category;exit();
            if($category && $tag){

                // $post = Post::create($input);
                // $post->update($input);
                $upost = Post::find($request->id);
                if($upost){
                    $upost->categories()->sync($category);
                    $upost->tags()->sync($tag);
                    $upost->delete();
                    return response()->json(['success' => 'Post deleted successfully!'], 200 );
                }else{
                    return response()->json(['error' => 'ID Not Found'], 400 );
                }
                

            }else{
                return response()->json(['error' => 'Category / Tag not found!'], 401 );

            }

        }
    }


}
