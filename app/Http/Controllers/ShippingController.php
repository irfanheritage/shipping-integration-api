<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Province;
use App\Models\City;
use App\Models\Courier;
use App\Models\Cost_calculation;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function provinces() {
     
        $provinces = Province::all();

        return response()->json($provinces);

    }

    public function show_province($id) {
        $province = Province::find($id);

        return response()->json($province);
    }

    public function couriers() {
     
        $couriers = Courier::all();

        return response()->json($couriers);

    }

     public function show_courier($id) {
        $courier = Courier::find($id);

        return response()->json($courier);
    }

    public function cities() {
     
        $cities = City::all();

        return response()->json($cities);

    }

    public function show_city($id) {
        $city = City::find($id);

        return response()->json($city);
    }

    public function show_city_per_province($province_id) {
        $city = City::where('province_id', '=', $province_id)->get();

        return response()->json($city);
    }

    public function create(Request $request){
        $origin = $request->origin;
        $destination = $request->destination;
        $weight = $request->weight;
        $courier = $request->courier;


        $qorigin = City::find($origin);
        $qdestination = City::find($destination);
        $qcourier = Courier::where('code', '=', $courier)->get();
        $qcourier_service = DB::table('courier_packages')->select('id')->where('code', '=', $courier)->get()->toArray();
        
        $cs = [];
        foreach($qcourier_service as $q){
            array_push($cs, $q->id);
        }
        // echo "<pre>";print_r($cs);exit();
        $qcosts = Cost_calculation::select('package_id', 'ppw')
                    ->where('origin', '=', $origin)
                    ->where('destination', '=', $destination)
                    ->whereIn('package_id', $cs)
                    ->get();
        
        $costfinal = [];
        for ($i = 0; $i<count($qcosts); $i++){
            // <p>This is user {{ $user->id }}</p>
            $service = DB::table('courier_packages')->find($qcosts[$i]->package_id);
            $costfinal['cost'][$i]['package_id'] = $service->id;
            $costfinal['cost'][$i]['service'] = $service->service;
            $costfinal['cost'][$i]['description'] = $service->description;
            $costfinal['cost'][$i]['etd'] = $service->etd;
            $costfinal['cost'][$i]['price'] = ceil($weight / 1000) * $qcosts[$i]->ppw;
        }
        $status['code'] = 200;
        $status['message'] = 'success';

        $dweight['original_weight'] = ($weight / 1000)." Kg";
        $dweight['calc_weight'] = ceil($weight / 1000)." Kg";

        $resp = [];
        $resp['status'] = $status;
        $resp['weight'] = $dweight;
        $resp['origin'] = $qorigin;
        $resp['destination'] = $qdestination;
        // $resp['courier'] = $qcourier;
        // $resp['qcost'] = $qcosts;
        $resp['calculation'] = $costfinal;
        $resp['calculation']['courier'] = $qcourier;
        
        
        
        
        return response()->json($resp);

        


    }


}
