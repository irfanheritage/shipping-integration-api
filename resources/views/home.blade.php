<?php 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>ZenBlog Bootstrap Template - Index</title>
    <meta name="robots" content="noindex, nofollow">
    <meta content="" name="description">
    <meta content="" name="keywords">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/favicon.png" rel="icon">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/apple-touch-icon.png" rel="apple-touch-icon">
    <link rel="preconnect" href="https://bootstrapmade.com/demo/templates/ZenBlog/https://fonts.googleapis.com">
    <link rel="preconnect" href="https://bootstrapmade.com/demo/templates/ZenBlog/https://fonts.gstatic.com" crossorigin>
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500&family=Inter:wght@400;500&family=Playfair+Display:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/aos/aos.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/css/variables.css" rel="stylesheet">
    <link href="https://bootstrapmade.com/demo/templates/ZenBlog/assets/css/main.css" rel="stylesheet">
  </head>
  <body>
    <header id="header" class="header d-flex align-items-center fixed-top">
      <div class="container-fluid container-xl d-flex align-items-center justify-content-between">
        <a href="https://bootstrapmade.com/demo/templates/ZenBlog/index.html" class="logo d-flex align-items-center">
          <h1>ZenBlog</h1>
        </a>
        <nav id="navbar" class="navbar">
          <ul>
            <li>
              <a href="https://bootstrapmade.com/demo/templates/ZenBlog/index.html">Blog</a>
            </li>
          </ul>
        </nav>
        <div class="position-relative" style="display:none">
          <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="mx-2">
            <span class="bi-facebook"></span>
          </a>
          <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="mx-2">
            <span class="bi-twitter"></span>
          </a>
          <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="mx-2">
            <span class="bi-instagram"></span>
          </a>
          <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="mx-2 js-search-open">
            <span class="bi-search"></span>
          </a>
          <i class="bi bi-list mobile-nav-toggle"></i>
          <div class="search-form-wrap js-search-form-wrap">
            <form action="search-result.html" class="search-form">
              <span class="icon bi-search"></span>
              <input type="text" placeholder="Search" class="form-control">
              <button class="btn js-search-close">
                <span class="bi-x"></span>
              </button>
            </form>
          </div>
        </div>
      </div>
    </header>
    <main id="main">
      <section id="posts" class="posts">
        <div class="container" data-aos="fade-up">
          <div class="row g-5">
            <div class="col-lg-4">
              <div class="post-entry-1 lg">
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                  <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-1.jpg" alt="" class="img-fluid">
                </a>
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">11 Work From Home Part-Time Jobs You Can Do Now</a>
                </h2>
                <p class="mb-4 d-block">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero temporibus repudiandae, inventore pariatur numquam cumque possimus exercitationem? Nihil tempore odit ab minus eveniet praesentium, similique blanditiis molestiae ut saepe perspiciatis officia nemo, eos quae cumque. Accusamus fugiat architecto rerum animi atque eveniet, quo, praesentium dignissimos</p>
                <div class="d-flex align-items-center author">
                  <div class="photo">
                    <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/person-1.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="name">
                    <h3 class="m-0 p-0">Cameron Williamson</h3>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="row g-5">
                <div class="col-lg-4 border-start custom-border">
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-2.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Sport</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Let’s Get Back to Work, New York</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-5.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Food</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 17th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-7.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Design</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Mar 15th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Why Craigslist Tampa Is One of The Most Interesting Places On the Web?</a>
                    </h2>
                  </div>
                </div>
                <div class="col-lg-4 border-start custom-border">
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-3.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Business</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">6 Easy Steps To Create Your Own Cute Merch For Instagram</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-6.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Tech</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Mar 1st '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">10 Life-Changing Hacks Every Working Mom Should Know</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-8.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Travel</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">5 Great Startup Tips for Female Founders</a>
                    </h2>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="trending">
                    <h3>Trending</h3>
                    <ul class="trending-post">
                      <li>
                        <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                          <span class="number">1</span>
                          <h3>The Best Homemade Masks for Face (keep the Pimples Away)</h3>
                          <span class="author">Jane Cooper</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                          <span class="number">2</span>
                          <h3>17 Pictures of Medium Length Hair in Layers That Will Inspire Your New Haircut</h3>
                          <span class="author">Wade Warren</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                          <span class="number">3</span>
                          <h3>13 Amazing Poems from Shel Silverstein with Valuable Life Lessons</h3>
                          <span class="author">Esther Howard</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                          <span class="number">4</span>
                          <h3>9 Half-up/half-down Hairstyles for Long and Medium Hair</h3>
                          <span class="author">Cameron Williamson</span>
                        </a>
                      </li>
                      <li>
                        <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                          <span class="number">5</span>
                          <h3>Life Insurance And Pregnancy: A Working Mom’s Guide</h3>
                          <span class="author">Jenny Wilson</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="category-section">
        <div class="container" data-aos="fade-up">
          <div class="section-header d-flex justify-content-between align-items-center mb-5">
            <h2>Culture</h2>
            <div>
              <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html" class="more">See All Culture</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
              <div class="d-lg-flex post-entry-2">
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html" class="me-4 thumbnail mb-4 mb-lg-0 d-inline-block">
                  <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-6.jpg" alt="" class="img-fluid">
                </a>
                <div>
                  <div class="post-meta">
                    <span class="date">Culture</span>
                    <span class="mx-1">&bullet;</span>
                    <span>Jul 5th '22</span>
                  </div>
                  <h3>
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</a>
                  </h3>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio placeat exercitationem magni voluptates dolore. Tenetur fugiat voluptates quas, nobis error deserunt aliquam temporibus sapiente, laudantium dolorum itaque libero eos deleniti?</p>
                  <div class="d-flex align-items-center author">
                    <div class="photo">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/person-2.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="name">
                      <h3 class="m-0 p-0">Wade Warren</h3>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4">
                  <div class="post-entry-1 border-bottom">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-1.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Culture</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">11 Work From Home Part-Time Jobs You Can Do Now</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                    <p class="mb-4 d-block">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero temporibus repudiandae, inventore pariatur numquam cumque possimus</p>
                  </div>
                  <div class="post-entry-1">
                    <div class="post-meta">
                      <span class="date">Culture</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">5 Great Startup Tips for Female Founders</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-2.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Culture</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                    <p class="mb-4 d-block">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero temporibus repudiandae, inventore pariatur numquam cumque possimus</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">17 Pictures of Medium Length Hair in Layers That Will Inspire Your New Haircut</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">9 Half-up/half-down Hairstyles for Long and Medium Hair</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Life Insurance And Pregnancy: A Working Mom’s Guide</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">The Best Homemade Masks for Face (keep the Pimples Away)</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Culture</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">10 Life-Changing Hacks Every Working Mom Should Know</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="category-section">
        <div class="container" data-aos="fade-up">
          <div class="section-header d-flex justify-content-between align-items-center mb-5">
            <h2>Business</h2>
            <div>
              <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html" class="more">See All Business</a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9 order-md-2">
              <div class="d-lg-flex post-entry-2">
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html" class="me-4 thumbnail d-inline-block mb-4 mb-lg-0">
                  <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-3.jpg" alt="" class="img-fluid">
                </a>
                <div>
                  <div class="post-meta">
                    <span class="date">Business</span>
                    <span class="mx-1">&bullet;</span>
                    <span>Jul 5th '22</span>
                  </div>
                  <h3>
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</a>
                  </h3>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio placeat exercitationem magni voluptates dolore. Tenetur fugiat voluptates quas, nobis error deserunt aliquam temporibus sapiente, laudantium dolorum itaque libero eos deleniti?</p>
                  <div class="d-flex align-items-center author">
                    <div class="photo">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/person-4.jpg" alt="" class="img-fluid">
                    </div>
                    <div class="name">
                      <h3 class="m-0 p-0">Wade Warren</h3>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-4">
                  <div class="post-entry-1 border-bottom">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-5.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Business</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">11 Work From Home Part-Time Jobs You Can Do Now</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                    <p class="mb-4 d-block">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero temporibus repudiandae, inventore pariatur numquam cumque possimus</p>
                  </div>
                  <div class="post-entry-1">
                    <div class="post-meta">
                      <span class="date">Business</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">5 Great Startup Tips for Female Founders</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                </div>
                <div class="col-lg-8">
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-7.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Business</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                    <p class="mb-4 d-block">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero temporibus repudiandae, inventore pariatur numquam cumque possimus</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Business</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Business</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">17 Pictures of Medium Length Hair in Layers That Will Inspire Your New Haircut</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Business</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">9 Half-up/half-down Hairstyles for Long and Medium Hair</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Business</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Life Insurance And Pregnancy: A Working Mom’s Guide</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Business</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">The Best Homemade Masks for Face (keep the Pimples Away)</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Business</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">10 Life-Changing Hacks Every Working Mom Should Know</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="category-section">
        <div class="container" data-aos="fade-up">
          <div class="section-header d-flex justify-content-between align-items-center mb-5">
            <h2>Lifestyle</h2>
            <div>
              <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html" class="more">See All Lifestyle</a>
            </div>
          </div>
          <div class="row g-5">
            <div class="col-lg-4">
              <div class="post-entry-1 lg">
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                  <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-8.jpg" alt="" class="img-fluid">
                </a>
                <div class="post-meta">
                  <span class="date">Lifestyle</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">11 Work From Home Part-Time Jobs You Can Do Now</a>
                </h2>
                <p class="mb-4 d-block">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vero temporibus repudiandae, inventore pariatur numquam cumque possimus exercitationem? Nihil tempore odit ab minus eveniet praesentium, similique blanditiis molestiae ut saepe perspiciatis officia nemo, eos quae cumque. Accusamus fugiat architecto rerum animi atque eveniet, quo, praesentium dignissimos</p>
                <div class="d-flex align-items-center author">
                  <div class="photo">
                    <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/person-7.jpg" alt="" class="img-fluid">
                  </div>
                  <div class="name">
                    <h3 class="m-0 p-0">Esther Howard</h3>
                  </div>
                </div>
              </div>
              <div class="post-entry-1 border-bottom">
                <div class="post-meta">
                  <span class="date">Lifestyle</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">The Best Homemade Masks for Face (keep the Pimples Away)</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
              <div class="post-entry-1">
                <div class="post-meta">
                  <span class="date">Lifestyle</span>
                  <span class="mx-1">&bullet;</span>
                  <span>Jul 5th '22</span>
                </div>
                <h2 class="mb-2">
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">10 Life-Changing Hacks Every Working Mom Should Know</a>
                </h2>
                <span class="author mb-3 d-block">Jenny Wilson</span>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="row g-5">
                <div class="col-lg-4 border-start custom-border">
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-6.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Let’s Get Back to Work, New York</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-5.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 17th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-4.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Mar 15th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Why Craigslist Tampa Is One of The Most Interesting Places On the Web?</a>
                    </h2>
                  </div>
                </div>
                <div class="col-lg-4 border-start custom-border">
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-3.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">6 Easy Steps To Create Your Own Cute Merch For Instagram</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-2.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Mar 1st '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">10 Life-Changing Hacks Every Working Mom Should Know</a>
                    </h2>
                  </div>
                  <div class="post-entry-1">
                    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                      <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-landscape-1.jpg" alt="" class="img-fluid">
                    </a>
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2>
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">5 Great Startup Tips for Female Founders</a>
                    </h2>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="post-entry-1 border-bottom">
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">How to Avoid Distraction and Stay Focused During Video Calls?</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                  <div class="post-entry-1 border-bottom">
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">17 Pictures of Medium Length Hair in Layers That Will Inspire Your New Haircut</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                  <div class="post-entry-1 border-bottom">
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">9 Half-up/half-down Hairstyles for Long and Medium Hair</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                  <div class="post-entry-1 border-bottom">
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">Life Insurance And Pregnancy: A Working Mom’s Guide</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                  <div class="post-entry-1 border-bottom">
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">The Best Homemade Masks for Face (keep the Pimples Away)</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                  <div class="post-entry-1 border-bottom">
                    <div class="post-meta">
                      <span class="date">Lifestyle</span>
                      <span class="mx-1">&bullet;</span>
                      <span>Jul 5th '22</span>
                    </div>
                    <h2 class="mb-2">
                      <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">10 Life-Changing Hacks Every Working Mom Should Know</a>
                    </h2>
                    <span class="author mb-3 d-block">Jenny Wilson</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer id="footer" class="footer">
      <div class="footer-content">
        <div class="container">
          <div class="row g-5">
            <div class="col-lg-4">
              <h3 class="footer-heading">About ZenBlog</h3>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam ab, perspiciatis beatae autem deleniti voluptate nulla a dolores, exercitationem eveniet libero laudantium recusandae officiis qui aliquid blanditiis omnis quae. Explicabo?</p>
              <p>
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/about.html" class="footer-link-more">Learn More</a>
              </p>
            </div>
            <div class="col-6 col-lg-2">
              <h3 class="footer-heading">Navigation</h3>
              <ul class="footer-links list-unstyled">
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/index.html">
                    <i class="bi bi-chevron-right"></i> Home </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/index.html">
                    <i class="bi bi-chevron-right"></i> Blog </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Categories </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html">
                    <i class="bi bi-chevron-right"></i> Single Post </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/about.html">
                    <i class="bi bi-chevron-right"></i> About us </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/contact.html">
                    <i class="bi bi-chevron-right"></i> Contact </a>
                </li>
              </ul>
            </div>
            <div class="col-6 col-lg-2">
              <h3 class="footer-heading">Categories</h3>
              <ul class="footer-links list-unstyled">
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Business </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Culture </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Sport </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Food </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Politics </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Celebrity </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Startups </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/category.html">
                    <i class="bi bi-chevron-right"></i> Travel </a>
                </li>
              </ul>
            </div>
            <div class="col-lg-4">
              <h3 class="footer-heading">Recent Posts</h3>
              <ul class="footer-links footer-blog-entry list-unstyled">
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html" class="d-flex align-items-center">
                    <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-sq-1.jpg" alt="" class="img-fluid me-3">
                    <div>
                      <div class="post-meta d-block">
                        <span class="date">Culture</span>
                        <span class="mx-1">&bullet;</span>
                        <span>Jul 5th '22</span>
                      </div>
                      <span>5 Great Startup Tips for Female Founders</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html" class="d-flex align-items-center">
                    <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-sq-2.jpg" alt="" class="img-fluid me-3">
                    <div>
                      <div class="post-meta d-block">
                        <span class="date">Culture</span>
                        <span class="mx-1">&bullet;</span>
                        <span>Jul 5th '22</span>
                      </div>
                      <span>What is the son of Football Coach John Gruden, Deuce Gruden doing Now?</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html" class="d-flex align-items-center">
                    <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-sq-3.jpg" alt="" class="img-fluid me-3">
                    <div>
                      <div class="post-meta d-block">
                        <span class="date">Culture</span>
                        <span class="mx-1">&bullet;</span>
                        <span>Jul 5th '22</span>
                      </div>
                      <span>Life Insurance And Pregnancy: A Working Mom’s Guide</span>
                    </div>
                  </a>
                </li>
                <li>
                  <a href="https://bootstrapmade.com/demo/templates/ZenBlog/single-post.html" class="d-flex align-items-center">
                    <img src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/img/post-sq-4.jpg" alt="" class="img-fluid me-3">
                    <div>
                      <div class="post-meta d-block">
                        <span class="date">Culture</span>
                        <span class="mx-1">&bullet;</span>
                        <span>Jul 5th '22</span>
                      </div>
                      <span>How to Avoid Distraction and Stay Focused During Video Calls?</span>
                    </div>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-legal">
        <div class="container">
          <div class="row justify-content-between">
            <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
              <div class="copyright"> © Copyright <strong>
                  <span>ZenBlog</span>
                </strong>. All Rights Reserved </div>
              <div class="credits"> Designed by <a href="https://bootstrapmade.com/demo/templates/ZenBlog/https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="social-links mb-3 mb-lg-0 text-center text-md-end">
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="twitter">
                  <i class="bi bi-twitter"></i>
                </a>
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="facebook">
                  <i class="bi bi-facebook"></i>
                </a>
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="instagram">
                  <i class="bi bi-instagram"></i>
                </a>
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="google-plus">
                  <i class="bi bi-skype"></i>
                </a>
                <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="linkedin">
                  <i class="bi bi-linkedin"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <a href="https://bootstrapmade.com/demo/templates/ZenBlog/#" class="scroll-top d-flex align-items-center justify-content-center">
      <i class="bi bi-arrow-up-short"></i>
    </a>
    <script src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/aos/aos.js"></script>
    <script src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/vendor/php-email-form/validate.js"></script>
    <script src="https://bootstrapmade.com/demo/templates/ZenBlog/assets/js/main.js"></script>
    <script async src='https://www.googletagmanager.com/gtag/js?id=G-P7JSYB1CSP'></script>
    <script>
      if (window.self == window.top) {
        window.dataLayer = window.dataLayer || [];

        function gtag() {
          dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-P7JSYB1CSP');
      }
    </script>
  </body>
</html>