<div class="sidebar" data-color="azure" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
    <div class="logo">
        <a href="/dashboard" class="simple-text logo-normal text-center">
            POS
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item {{url()->current() == url('dashboard') ? 'active': ''}} ">
            <a class="nav-link" href="{{ url('dashboard')}}">
                <i class="material-icons">dashboard</i>
                <p>Dashboard</p>
            </a>
            </li>
            <li class="nav-item {{url()->current() == url('merchants') ? 'active': ''}}">
            <a class="nav-link" href="{{url('merchants')}}">
                <i class="material-icons">storefront</i>
                <p>Merchants</p>
            </a>
            </li>
            <li class="nav-item {{url()->current() == url('employees') ? 'active': ''}}">
            <a class="nav-link" href="{{url('employees')}}">
                <i class="material-icons">people_alt</i>
                <p>Employees</p>
            </a>
            </li>
            <li class="nav-item {{url()->current() == url('items') ? 'active': ''}}">
            <a class="nav-link" href="{{url('items')}}">
                <i class="material-icons">shopping_bag</i>
                <p>Items</p>
            </a>
            </li>
            <li class="nav-item ">
                <a class="nav-link collapsed" data-toggle="collapse" href="#pagesExamples" aria-expanded="false">
                  <i class="material-icons">apps</i>
                  <p> Master Data
                    <b class="caret"></b>
                  </p>
                </a>
                <div class="collapse" id="pagesExamples" style="">
                  <ul class="nav">
                    <li class="nav-item ">
                      <a class="nav-link" href="./merchants">
                        <span class="sidebar-mini"> M </span>
                        <span class="sidebar-normal"> Merchants </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
        </ul>
    </div>  
</div>