<?php
use Psr\Http\Message\ServerRequestInterface;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
// Route::get('/home', function () {
//     return view('home');
// });


$router->get('/version', function () use ($router) {
    return $router->app->version();
    // return view('home');
});

Route::get('/', 'HomeController@index');

$router->group([ 'middleware' => 'auth'], function () use ($router) {
    
    /* get user profile */
    $router->get('/getDataUser', [ 'as' => 'getDataUser', 'uses' => 'AuthController@me']);

    /* logout user */
    $router->post('/logout', [ 'as' => 'logout', 'uses' => 'AuthController@logout']);

    /* refresh token */
    // $router->get('/refresh-token', [ 'as' => 'refreshToken', 'uses' => 'AuthController@refresh']);
});

Route::group([
    'prefix' => 'api/v1'

], function ($router) {    
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    

});

/* restrict route */

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'api/v1'

], function ($router) {
    // Route::post('logout', 'AuthController@logout');
    // Route::post('refresh', 'AuthController@refresh');
    // Route::post('getDataUser', 'AuthController@me');

    Route::get('provinces', 'ShippingController@provinces');
    Route::get('province/{id}', 'ShippingController@show_province');

    Route::get('couriers', 'ShippingController@couriers');
    Route::get('courier/{id}', 'ShippingController@show_courier');

    Route::get('cities', 'ShippingController@cities');
    Route::get('city/{id}', 'ShippingController@show_city');
    Route::get('city/province/{province_id}', 'ShippingController@show_city_per_province');

    Route::post('cost', 'ShippingController@create');

    Route::get('posts', 'PostController@allposts');
    Route::get('post/{id}', 'PostController@show_post');
    Route::get('post/showByTag/{id}', 'PostController@show_post_by_tag');
    Route::get('post/showByCategory/{id}', 'PostController@show_post_by_category');
    Route::post('post/create', 'PostController@store');
    Route::post('post/update', 'PostController@update');
    Route::post('post/delete', 'PostController@destroy');

    Route::get('categories', 'CategoryController@allcategories');
    Route::get('category/{id}', 'CategoryController@show_category');
    Route::post('category/create', 'CategoryController@store');
    Route::post('category/update', 'CategoryController@update');
    Route::post('category/delete', 'CategoryController@destroy');
    
    Route::get('tags', 'TagController@alltags');
    Route::get('tag/{id}', 'TagController@show_tag');
    Route::post('tag/create', 'TagController@store');
    Route::post('tag/update', 'TagController@update');
    Route::post('tag/delete', 'TagController@destroy');
});